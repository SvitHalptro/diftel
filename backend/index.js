import express from "express";
import cors from "cors";
import db from "./database/db.js";
import {routerContacts, routerGetProfesionals, routerMeetings, routerMessages, routerProfesionProfesionales } from './routes/routes.js';
import { ConferencesModel, ContactsModel, MessageModel, ProfesionProfesionalModel } from "./models/appBlogModel.js";

const app = express();

app.use(cors());
app.use(express.json());
app.use('/contacts', routerContacts);
app.use('/profession', routerProfesionProfesionales);
app.use('/getProfesionals', routerGetProfesionals);
app.use('/getMessages', routerMessages);
app.use('/getMeetings', routerMeetings);

try {     
    await db.authenticate();
    console.log('Conexión exitosa')
} catch (error) {
        
    console.log('El error es ' + error);
}

ProfesionProfesionalModel.belongsTo(ContactsModel, {foreignKey: 'idprofesional'});
MessageModel.belongsTo(ContactsModel, {foreignKey: 'idEmisor', foreignKey:'idReceptor'});
ConferencesModel.belongsTo(ContactsModel, {foreignKey: 'id_profesional', foreignKey:'id_client'});

app.get('/', (req, res) => {
    res.send("Hello world");
})

app.listen(8000, ()=>{
    console.log("serverCreate in port 8000");
});