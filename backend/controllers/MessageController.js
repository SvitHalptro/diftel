import { MessageModel } from "../models/appBlogModel.js"
import { Op } from "sequelize"

export const getMessages = async (req, res) => { // contacto emisor, contacto receptor
    try {
        const message = await MessageModel.findAll({
            where:
            {
                [Op.or]: [
                    {
                        [Op.and]: { idEmisor: req.params.idEmisor, idReceptor: req.params.idReceptor }
                    }, {
                        [Op.and]: { idEmisor: req.params.idReceptor, idReceptor: req.params.idEmisor }
                    }
                ]
            }
        })

        res.json(message);
    } catch (error) {
        res.json({ message: error.message });
    }
}

export const newMessage = async (req, res) => {
    try {
        await MessageModel.create(req.body);
        res.json({ text: 'Mensaje registrado' });

    } catch (error) {
        res.json({ message: error.message });
    }
}