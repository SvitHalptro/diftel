import { ConferencesModel } from "../models/appBlogModel.js";

export const newMeeting = async (req, res) => {
    try{
        await ConferencesModel.create(req.body);
         res.json({message: "registro creado correctamente"});
    }catch(error){
        res.json({message: error.message});
    }
}

export const getAllMeeting = async (req, res) => {
    try{
        const conferences = await ConferencesModel.findAll();
        res.json(conferences);
    }catch(error){
        res.json({message: error.message});
    }
}


export const getMeetings = async (req, res) => { // contacto emisor, contacto receptor
    try {
        const message = await ConferencesModel.findAll({
            where:
            {
                [Op.or]: [
                    {
                        [Op.and]: { id_profesional: req.params.id_profesional, id_client: req.params.id_client }
                    }, {
                        [Op.and]: { id_profesional: req.params.id_client, id_client: req.params.id_profesional }
                    }
                ]
            }
        })

        res.json(message);
    } catch (error) {
        res.json({ message: error.message });
    }
}

