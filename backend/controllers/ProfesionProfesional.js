import { ProfesionProfesionalModel } from "../models/appBlogModel.js";

export const getAllProfesionals = async (req, res) => {
    try{
        const profesionals = await ProfesionProfesionalModel.findAll();
        res.json(profesionals);
    }catch(error){
        res.json({message: error.message});
    }
}

export const getProfesional = async (req, res) => {
    try{
        const profesional = await ProfesionProfesionalModel.findAll({
            where: {id: req.params.id}
        });
        res.json(profesional);
    }catch(error){
        res.json({message: error.message});
    }
}

export const newProfesional = async (req, res) => {
    try{
        await ProfesionProfesionalModel.create(req.body);
        await res.json({message: "registro creado correctamente"});
    }catch(error){
        res.json({message: error.message});
    }
}

export const updateProfesional = async (req, res) =>{
    try
    {
        await ProfesionProfesionalModel.update({
            where: {id: req.params.id}
        });
        await res.json({message: "Registro actualizado correctamente"})
    }catch(error){
        res.json({message: error.message});
    }
}

export const destroyProfesional = async (req, res) =>{
    try {
        await ProfesionProfesionalModel.destroy({where: {id: req.params.id}});
    } catch (error) {
        res.json({message:error.message});
    }
}