import { ContactsModel } from './../models/appBlogModel.js'

export const getAllContacts = async (req, res) => {
    try {
        const contacts = await ContactsModel.findAll();
        res.json(contacts);
    } catch (error) {
        res.json({ message: error.message });
    }
}

export const getContact = async (req, res) => { //for id
    try {
        const contact = await ContactsModel.findAll({
            where: { id: req.params.id }
        });
        res.json(contact);
    } catch (error) {
        res.json({ message: error.message });
    }
}

export const getContactProfesional = async (req, res) => { //for tipo:'P'
    try {
        const contact = await ContactsModel.findAll({
            where: { "tipoCuenta": "P" }
        });
        console.log("Todo bien hasta aqui");
        res.json(contact);
    } catch (error) {
        res.json({ message: error.message });
    }
}

export const getContactUserPassword = async (req, res) => { //for id
    try {
        const contact = await ContactsModel.findAll({
            where: { correo: req.params.correo, clave: req.params.clave }
        });
        res.json(contact);
    } catch (error) {
        res.json({ message: error.message });
    }
}

export const getExistUser = async (req, res) => {
    try {
        const contacts = await ContactsModel.findAll({
            where: { correo: req.params.correo }
        });
        res.json(contacts);
        
    } catch (error) {
        res.json({ message: error.message });
    }
}

export const newContact = async (req, res) => {
    try {
        await ContactsModel.create(req.body);
        // res.json(res.json({message: "registro creado correctamente"}));
        res.json({ message: "registro creado correctamente" });
    } catch (error) {
        res.json({ message: error.message });
    }
}

export const updateContact = async (req, res) => {
    try {
        await ContactsModel.update({
            where: { id: req.params.id }
        });
        res.json({ message: "Registro actualizado correctamente" })
    } catch (error) {
        res.json({ message: error.message });
    }
}

export const destroyContact = async (req, res) => {
    try {
        await ContactsModel.destroy({ where: { id: req.params.id } });
    } catch (error) {
        res.json({ message: error.message });
    }
}
