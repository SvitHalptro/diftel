import db from "./../database/db.js";
import { DataTypes } from "sequelize";

export const ContactsModel = db.define('contacts', {
    id: {type: DataTypes.NUMBER, primaryKey: true},
    nombre: {type: DataTypes.STRING},
    apellidoPaterno: {type: DataTypes.STRING},
    apellidoMaterno: {type: DataTypes.STRING},
    dni: {type: DataTypes.STRING},
    residenciaActual: {type: DataTypes.STRING},
    telefono: {type: DataTypes.STRING},
    sexo: {type: DataTypes.CHAR},
    nacionalidad: {type: DataTypes.STRING},
    estadoCivil: {type: DataTypes.CHAR},
    correoPersonal: {type: DataTypes.STRING},
    fechaNacimiento: {type: DataTypes.DATE}, 
    tipoCuenta: {type: DataTypes.CHAR},
    correo: {type: DataTypes.STRING},
    clave: {type: DataTypes.STRING} 
});
export const ProfesionProfesionalModel = db.define('profesionprofesionals', {
    id: {type:DataTypes.NUMBER, primaryKey: true},
    nombre: {type:DataTypes.STRING},
    categoria: {type:DataTypes.STRING},
    aniosExperiencia: {type: DataTypes.NUMBER},
    escala: {type: DataTypes.STRING},
    idprofesional: {type:DataTypes.INTEGER.UNSIGNED}	
});

export const MessageModel = db.define('messages', {
    id: {type:DataTypes.NUMBER, primaryKey: true},
    mensaje: {type:DataTypes.STRING},
    fecha: {type:DataTypes.DATE},
    idEmisor: {type: DataTypes.INTEGER.UNSIGNED},
    idReceptor: {type: DataTypes.INTEGER.UNSIGNED}
});

export const ConferencesModel = db.define('meetings',{
    id: {type: DataTypes.NUMBER, primaryKey: true},
    fechareunion:{type:DataTypes.DATE},
    id_profesional:{type:DataTypes.INTEGER.UNSIGNED},
    id_client:{type:DataTypes.INTEGER.UNSIGNED}
});