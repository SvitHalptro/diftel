import express from "express";
import { destroyContact, getAllContacts, getContact, getContactProfesional, getContactUserPassword, getExistUser, newContact, updateContact } from "../controllers/contactController.js";
import { getMeetings, newMeeting } from "../controllers/MeetingDates.js";
import { getMessages, newMessage } from "../controllers/MessageController.js";
import { destroyProfesional, getAllProfesionals, getProfesional, newProfesional, updateProfesional } from "../controllers/ProfesionProfesional.js";

export const routerContacts = express.Router();
routerContacts.get('/', getAllContacts);
routerContacts.get('/user/:correo/consult/:clave', getContactUserPassword);
routerContacts.get('/veriffy/:correo', getExistUser);
routerContacts.get('/:id', getContact);
routerContacts.post('/', newContact);
routerContacts.put('/:id', updateContact);
routerContacts.delete('/', destroyContact);
//Bautista Ruiz Cristhian Alexander
export const routerProfesionProfesionales = express.Router();
routerProfesionProfesionales.get('/', getAllProfesionals);
routerProfesionProfesionales.get('/:id', getProfesional);
routerProfesionProfesionales.post('/', newProfesional);
routerProfesionProfesionales.put('/:id', updateProfesional);
routerProfesionProfesionales.delete('/', destroyProfesional);

export const routerGetProfesionals = express.Router();
routerGetProfesionals.get('/getProfesionales/', getContactProfesional);

export const routerMessages = express.Router();
routerMessages.get('/code/:idEmisor/cons/:idReceptor', getMessages);
routerMessages.post('/', newMessage);

export const routerMeetings = express.Router();
routerMeetings.get('/code/:id_profesional/cons/:id_client', getMeetings);
routerMeetings.post('/', newMeeting);
