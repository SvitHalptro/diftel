export class DocumentoIdentidad {
    DocumentoIdentidad(numeroIdentidad, primerApellido, segundoApellido, preNombres, fechaNacimiento, sexo, estadoCivil, nacionalidad) {
        this.numeroIdentidad = numeroIdentidad;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.preNombres = preNombres;
        this.fechaNacimiento = fechaNacimiento;
        this.sexo = sexo.charAt(0);
        this.estadoCivil = estadoCivil.charAt(0);
        this.nacionalidad = nacionalidad;
    }
    getNumeroIdentidad() { 
        return this.numeroIdentidad;
    }
    setNumeroIdentidad(numeroIdentidad) {
        this.numeroIdentidad = numeroIdentidad;
    }
    getPrimerApellido() {
        return this.primerApellido;
    }
    setPrimerApellido(primerApellido) {
        this.primerApellido = primerApellido;
    }
    getSegundoApellido() {
        return this.segundoApellido;
    }
    setSegundoApellido(segundoApellido) {
        this.segundoApellido = segundoApellido;
    }
    getPreNombres() {
        return this.preNombres;
    }
    setPreNombres(preNombres) {
        this.preNombres = preNombres;
    }
    getFechaNacimiento() {
        return this.fechaNacimiento;
    }
    setFechaNacimiento(fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    getSexo() {
        return this.sexo;
    }
    setSexo(sexo) {
        this.sexo = sexo.charAt(0);
    }
    getEstadoCivil() {
        return this.estadoCivil;
    }
    setEstadoCivil(estadoCivil) {
        this.estadoCivil = estadoCivil.charAt(0);
    }
    getNacionalidad() {
        return this.nacionalidad;
    }
    setNacionalidad(nacionalidad) {
        this.nacionalidad = nacionalidad;
    }
}