import { Usuario } from "./Usuario";
export class Cliente extends Usuario {
    constructor() {
        super(...arguments);
        this.profesionales = [];
    }
    Cliente(profesionales, documentoIdentidad, numeroCelular, correoPersonal, correo, clave) {
        super.Usuario(documentoIdentidad, numeroCelular, correoPersonal, correo, clave);
        this.profesionales = profesionales;
    }
    getProfesionales() {
        return this.profesionales;
    }
    setProfesionales(profesionales) {
        this.profesionales = profesionales;
    }
}