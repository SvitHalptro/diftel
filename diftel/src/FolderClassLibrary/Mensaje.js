class Mensaje {
    Mensaje(mensaje, fecha) {
        this.mensaje = mensaje;
        this.fecha = fecha;
    }
    getMensaje() {
        return this.mensaje;
    }
    setMensaje(mensaje) {
        this.mensaje = mensaje;
    }
    getfecha() {
        return this.fecha;
    }
    setfecha(fecha) {
        this.fecha = fecha;
    }
}