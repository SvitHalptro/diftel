import { Profesion } from "./Profesion";
import { Cliente } from "./Cliente";
import { DocumentoIdentidad } from "./DocumentoIdentidad";
import { Usuario } from "./Usuario";

export class Profesional extends Usuario {
    constructor() {
        super(...arguments); 
        this.profesiones = [];
        this.clientes = [];
    }
    // public Profesional( profesiones: Profesion[],  clientes:Cliente[]) {
    //     this.profesiones = profesiones;
    //     this.clientes = clientes;
    // }
    Profesional(profesiones, clientes, documentoIdentidad, numeroCelular, correoPersonal, correo, clave) {
        super.setDocumentoIdentidad(documentoIdentidad);
        super.setNumeroCelular(numeroCelular);
        super.setCorreoPersonal(correoPersonal);
        super.setCorreo(correo);
        super.setClave(clave);
        this.profesiones = profesiones;
        this.clientes = clientes;
    }
    getProfesiones() {
        return this.profesiones;
    }
    setProfesiones(profesiones) {
        this.profesiones = profesiones;
    }
    getClientes() {
        return this.clientes;
    }
    setClientes(clientes) {
        this.clientes = clientes;
    }
    getDocumentoIdentidad() {
        return super.documentoIdentidad;
    }
}