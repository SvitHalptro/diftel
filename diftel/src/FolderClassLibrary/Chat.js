import { Mensaje } from "./Mensaje";
export class Chat {
    constructor() {
        this.mensajesEnviados = [];
        this.mensajesRecibidos = [];
    }
    Chat(mensajesEnviados, mensajesRecibidos) {
        this.mensajesEnviados = mensajesEnviados;
        this.mensajesRecibidos = mensajesRecibidos;
    }
    getMensajesEnviados() {
        return this.mensajesEnviados;
    }
    setMensajesEnviados(mensajesEnviados) {
        this.mensajesEnviados = mensajesEnviados;
    }
    getMensajesRecibidos() {
        return this.mensajesRecibidos;
    }
    setMensajesRecibidos(mensajesRecibidos) {
        this.mensajesRecibidos = mensajesRecibidos;
    }
    
}