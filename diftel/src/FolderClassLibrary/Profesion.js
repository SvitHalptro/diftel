export class Profesion {
    Profesion(nombre, categoria, aniosExperiencia, escala) {
        this.nombre = nombre;
        this.categoria = categoria;
        this.aniosExperiencia = aniosExperiencia;
        this.escala = escala;
    } 
    getNombre() {
        return this.nombre;
    }
    setNombre(nombre) {
        this.nombre = nombre;
    }
    getCategoria() {
        return this.categoria;
    }
    setCategoria(categoria) {
        this.categoria = categoria;
    }
    getAniosExperiencia() {
        return this.aniosExperiencia;
    }
    setAniosExperiencia(aniosExperiencia) {
        this.aniosExperiencia = aniosExperiencia;
    }
    getEscala() {
        return this.escala;
    }
    setEscala(escala) {
        this.escala = escala;
    }
}