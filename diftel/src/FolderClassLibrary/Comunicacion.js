import { Chat } from "./Chat";
import { Cliente } from "./Cliente";
import { Profesion } from "./Profesion";

export class Comunicacion {
    constructor() {
        this.fechaCitas = [];
    }
    Comunicacion(id, profesion, cliente, calificacion, fechaCitas, costoConsulta, nombreConsulta, chat) {
        this.id = id;
        this.profesion = profesion;
        this.cliente = cliente;
        this.calificacion = calificacion;
        this.fechaCitas = fechaCitas;
        this.costoConsulta = costoConsulta;
        this.nombreConsulta = nombreConsulta;
        this.chat = chat;
    }
    getId() {
        return this.id;
    }
    setId(id) {
        this.id = id;
    }
    getProfesion() {
        return this.profesion;
    }
    setProfesion(profesion) {
        this.profesion = profesion;
    }
    getCliente() {
        return this.cliente;
    }
    setCliente(cliente) {
        this.cliente = cliente;
    }
    getCalificacion() {
        return this.calificacion;
    }
    setCalificacion(calificacion) {
        this.calificacion = calificacion;
    }
    getFechaCitas() {
        return this.fechaCitas;
    }
    setFechaCitas(fechaCitas) {
        this.fechaCitas = fechaCitas;
    }
    getCostoConsulta() {
        return this.costoConsulta;
    }
    setCostoConsulta(costoConsulta) {
        this.costoConsulta = costoConsulta;
    }
    getNombreConsulta() {
        return this.nombreConsulta;
    }
    setNombreConsulta(nombreConsulta) {
        this.nombreConsulta = nombreConsulta;
    }
    getChat() {
        return this.chat;
    }
    setChat(chat) {
        this.chat = chat;
    }
}