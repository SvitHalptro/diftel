"use strict";
export class Usuario {
    Usuario(documentoIdentidad, numeroCelular, correoPersonal, correo, clave) {
        this.documentoIdentidad = documentoIdentidad;
        this.numeroCelular = numeroCelular;
        this.correoPersonal = correoPersonal;
        this.correo = correo; 
        this.clave = clave;
    }
    getDocumentoIdentidad() {
        return this.documentoIdentidad;
    }
    setDocumentoIdentidad(documentoIdentidad) {
        this.documentoIdentidad = documentoIdentidad;
    }
    getNumeroCelular() {
        return this.numeroCelular;
    }
    setNumeroCelular(numeroCelular) {
        this.numeroCelular = numeroCelular;
    }
    getCorreoPersonal() {
        return this.correoPersonal;
    }
    setCorreoPersonal(correoPersonal) {
        this.correoPersonal = correoPersonal;
    }
    getCorreo() {
        return this.correo;
    }
    setCorreo(correo) {
        this.correo = correo;
    }
    getClave() {
        return this.clave;
    }
    setClave(clave) {
        this.clave = clave;
    }
}