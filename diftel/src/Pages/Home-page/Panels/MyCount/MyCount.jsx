import React, { useEffect, useState } from "react";
import "./style.css";
import imgSilueta from './../../../../src/img/iconLog.png'
import Label_interactive from "../../../../Components-design/Components-unic/Label_interactive";

import axios from "axios";

import Cookies from 'universal-cookie';
import { Usuario } from "../../../../FolderClassLibrary/Usuario";
import { Profesion } from "../../../../FolderClassLibrary/Profesion";

const URL = "http://localhost:8000/";
const URLcontacts = `${URL}contacts/`;
const URLprofesionals = `${URL}profession/`;


function MyCount({ contactActual_instance, setContactActual_instance }) {

    const [contactoActual, setContactoActual] = useState([new Usuario]);
    const [profesiones, setContactoProfesiones] = useState([new Profesion]);
    const cookies = new Cookies();

    useEffect(() => {
        getContactInfo();
    }, []);

    const getContactInfo = async () => {
        const user_json = await axios.get(URLcontacts + cookies.get('idUser'));
        if (user_json.data[0].tipoCuenta == 'P') {
            const user_profesion_json = await axios.get(URLprofesionals + cookies.get('idUser'));
            setContactoProfesiones(user_profesion_json.data);
        }

        setContactoActual(user_json.data[0]);
    }

    // const getContactsProfesionals = async () => {
    //     const user_json = await axios.get(URLcontacts + cookies.get('idUser'));
    //     let resc = null;
    //     if (user_json.data[0].tipoCuenta == 'P') {
    //         resc = await axios.get(URLcontacts);
    //         const user_profesion_json = await axios.get(URLprofesionals + cookies.get('idUser'));
    //         setContactoProfesiones(user_profesion_json.data);
    //     } else {
    //         resc = await axios.get(URL + 'getProfesionals/getProfesionales/');
    //     }
    //     // setContactoProfesiones.

    //     setContactoActual(resc.data[0]);
    // }

    return (
        <div className="totalMyCount_Panel_MyCount">
            <div>
                <img src={imgSilueta} alt="" />
                <p>{contactoActual.nombre} {contactoActual.apellidoPaterno} {contactoActual.apellidoMaterno}</p>
                <div className="row1">
                    <Label_interactive
                        text_label={"Nombre"}
                        valueText={contactoActual.nombre} />
                    <Label_interactive
                        text_label={"Apellido paterno"}
                        valueText={contactoActual.apellidoPaterno} />
                    <Label_interactive
                        text_label={"Apellido materno"}
                        valueText={contactoActual.apellidoMaterno} />

                </div>
                <div className="row1">
                    <Label_interactive
                        text_label={"Dni"}
                        valueText={contactoActual.dni} />
                    <Label_interactive
                        text_label={"Residencia actual"}
                        valueText={contactoActual.residenciaActual} />
                    <Label_interactive
                        text_label={"Teléfono"}
                        valueText={contactoActual.telefono} />

                </div>
                <div className="row1">
                    <Label_interactive
                        text_label={"Sexo"}
                        valueText={(contactoActual.sexo == 'M') ? 'Masculino' : ((contactActual_instance.sexo == 'F') ? 'Femenino' : 'No binario')} />
                    <Label_interactive
                        text_label={"Nacionalidad"}
                        valueText={contactoActual.nacionalidad} />
                    <Label_interactive
                        text_label={"Estado civil"}
                        valueText={(contactoActual.estadoCivil == 'S') ? 'Soltero(a)' : ((contactActual_instance.estadoCivil == 'C') ? "Casado(a)" : ((contactActual_instance.estadoCivil == 'V') ? "Viudo(a)" : "Divorsiado(a)"))} />

                </div>
                <div className="row1">
                    <Label_interactive
                        text_label={"Correo personal"}
                        valueText={contactoActual.correoPersonal} />
                    <Label_interactive
                        text_label={"Fecha de nacimiento"}
                        valueText={contactoActual.fechaNacimiento} />
                    <Label_interactive
                        text_label={"Tipo de cuenta"}
                        valueText={(contactoActual.tipoCuenta == 'P') ? 'Profesional' : 'Cliente'} />

                </div>
                {
                    profesiones.map((profession) => {
                        <div className="profesionalApartado">
                            <Label_interactive
                                text_label={"Profesión"}
                                valueText={profession.nombre} />
                            <Label_interactive
                                text_label={"Categoría"}
                                valueText={profession.categoria} />
                            <Label_interactive
                                text_label={"Escala"}
                                valueText={profession.escala} />
                        </div>
                    })
                }

                <div>
                    <Label_interactive
                        text_label={"Correo"}
                        valueText={contactoActual.correo} />
                    <Label_interactive
                        text_label={"Contraseña"}
                        valueText={contactoActual.clave} />
                </div>
            </div>
        </div>
    );
}
export default MyCount;