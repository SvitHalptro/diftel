import React, { useEffect, useState } from "react";
import { ChatPanel_Components } from "../../../../Components-design/Chat-componetns-messages/Chat_interf/ChatPanel_Components";
import Contact_component from "../../../../Components-design/Components-chat/Contact_component";
import axios from "axios";
import './style.css'

import Cookies from 'universal-cookie';
import { Usuario } from "../../../../FolderClassLibrary/Usuario";

const URL = "http://localhost:8000/";
const URLcontacts = `${URL}contacts/`;
const URLprofesionals = `${URL}profession/`;
const URLmeetings = `${URL}getMeetings/`;



function Explorer({ contactActual_instance, setContactActual_instance }) {


    const [contactos, setContactos] = useState([]);
    const [idContacto_seleccionado, setIdContacto_seleccionado] = useState(-1);
    const [contactoActualMessage, setContactoActualMessage] = useState([]);


    const [meetings_total, setMeetings_total] = useState([]);

    const getMeetings = async () => {
        const messages_json = await axios.get(`${URLmeetings}code/${cookies.get('idUser')}/cons/${idContacto_seleccionado}`);
        setMeetings_total(messages_json.data);
        console.log(meetings_total);
        console.log("Tamaño: " + meetings_total.length);
    }

    // const actualizar = setInterval(getMeetings, 500);

    useEffect(() => {
        getContactsProfesionals();
    }, []);

    const getContactsProfesionals = async () => {
        const user_json = await axios.get(URLcontacts + cookies.get('idUser'));
        let resc = null;
        if (user_json.data[0].tipoCuenta == 'P') {
            resc = await axios.get(URLcontacts);
        } else {
            resc = await axios.get(URL + 'getProfesionals/getProfesionales/');
        }
        setContactos(resc.data);
    }

    // const AparecMessages = ()=> {

    // }
    const methodContactClick1 = async (idContact_select) => {
        methodContactClick(idContact_select);

        getMeetings();
        getTotalMessages();

    }
    const methodContactClick = async (idContact_select) => {
        setIdContacto_seleccionado(idContact_select);
        const resc = await axios.get(URLcontacts + idContact_select);
        setContactoActualMessage(resc.data);

        // getTotalMessages();
    }





    // Sobre los mensajes
    const [totalMessages, setTotalMessages] = useState([]);
    const cookies = new Cookies;

    const getTotalMessages = async () => {
        if (idContacto_seleccionado != -1) {
            const messages_json = await axios.get(`http://localhost:8000/getMessages/code/${cookies.get('idUser')}/cons/${idContacto_seleccionado}`);
            setTotalMessages(messages_json.data);
        }
    }

    return (
        <div className="totalExplorer_Panel_Explorer">
            <div>
                <div className="explore_Panel_Explorer">
                    {contactoActualMessage.map((contact) => (
                        <ChatPanel_Components
                            Nombre={contact.nombre}
                            apellidos={contact.apellidoPaterno + ' ' + contact.apellidoMaterno}
                            id={contact.id}
                            methodActualizarMensajes={getTotalMessages}
                            totalMessages={totalMessages}
                            meetings_total={meetings_total} />
                    ))}
                </div>

                <div className="contacts_Panel_Explorer">
                    <p>Contactos</p>

                    {contactos.map((contact) => (
                        <Contact_component
                            nameContact={contact.nombre + ' ' + contact.apellidoPaterno + ' ' + contact.apellidoMaterno}
                            idContact={contact.id}
                            methodContactClick={methodContactClick1} />
                    ))}
                </div>
            </div>
        </div>
    );
}
export default Explorer;
