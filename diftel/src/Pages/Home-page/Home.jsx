import React, { useEffect } from "react";
import ComponentHeader from "../../Components-design/Header/ComponentHeader";
import MyCount from "./Panels/MyCount/MyCount";
import { useState } from "react";
import Explorer from "./Panels/Explorer/Explorer";
import Cookies from "universal-cookie";
import { Usuario } from "../../FolderClassLibrary/Usuario";
import { json } from "react-router-dom";
import { Profesional } from "../../FolderClassLibrary/Profesional";
import { DocumentoIdentidad } from "../../FolderClassLibrary/DocumentoIdentidad";
import { Profesion } from "../../FolderClassLibrary/Profesion";
import axios from "axios";
import { Cliente } from "../../FolderClassLibrary/Cliente";

function Home() {

    const URL = "http://localhost:8000/";
    const URLcontacts = `${URL}contacts/`;
    const URLprofesionals = `${URL}profession/`;

    const [panelActiveNumber, setpanelActiveNumber] = useState(1);

    const [contactActual_instance, setContactActual_instance] = useState(new Usuario);
    const [contactos, setContactos] = useState(new Usuario);
    const cookies = new Cookies();

    useEffect(() => {
        declarateUser();
    }, []);

    const declarateUser = async () => {
        console.log(cookies.get('idUser') );
        if (cookies.get('idUser') != null) {

            const user_json = await axios.get(URLcontacts + cookies.get('idUser'));
            const us = user_json.data[0];
            const documentoId = new DocumentoIdentidad(us.dni, us.apellidoPaterno, us.apellidoMaterno, us.nombre, us.fechaNacimiento, us.sexo, us.estadoCivil, us.nacionalidad);
            if (us.tipoUsuario == "P") {

                const user_profesion_json = await axios.get(URLprofesionals + documentoId.numeroIdentidad);
                const pro = user_profesion_json.data[0];

                const profesion = new Profesion(pro.nombre, pro.categoria, pro.aniosExperiencia, pro.escala);

                setContactActual_instance(new Profesional([profesion], [], documentoId, us.telfono, us.correoPersonal, us.correo, us.clave));
            }
            else {
                setContactActual_instance(new Cliente([], documentoId, us.telfono, us.correoPersonal, us.correo, us.clave));
            }
        }
        else{
            window.location.href = './';
        }
    }

    // 1: Panel Principal 
    // 2: Panel Mi cuenta
    const panelActive = numClic => {
        setpanelActiveNumber(numClic);
    }

    return (
        <div>
            <ComponentHeader
                numPress={panelActiveNumber}
                functionCambPanel={panelActive} />
            {(panelActiveNumber == 1) ? <Explorer contactActual_instance={contactActual_instance} setContactActual_instance={setContactActual_instance} /> : <MyCount contactActual_instance={contactActual_instance} setContactActual_instance={setContactActual_instance} />}
        </div>
    );
}
export default Home;