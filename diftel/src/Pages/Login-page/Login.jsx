import React, { useState } from "react";
import './src/styles.css'
import Input_interactive from "../../Components-design/Components-unic/Input_interactive";
import { async } from "q";
import axios from "axios";
import Cookies from 'universal-cookie';

const URL = "http://localhost:8000/";
const URLcontacts = `${URL}contacts/`;
const URLprofesionals = `${URL}profession/`;


function Login() {

    const [correo, setCorreo] = useState('');
    const [clave, setClave] = useState('');
    const [textIfNotExistCorreo, setTextIfNotExistCorreo] = useState('');


    const verifyCount = async (e) => {
        e.preventDefault();
        
        const encontrado = await axios.get(URLcontacts + "user/"+correo+"/consult/"+clave);
        const dataCont = encontrado.data;
        if (Object.keys(dataCont).length == 0) {
            setTextIfNotExistCorreo('El correo o la contraseña están incorrectos');
        } else{
            const cookies = new Cookies();
            cookies.set('idUser',  dataCont[0].id, {path: '/'});
            window.location.href = './Home';
        }
    }


    return (
        <div className="Login-page-root">
            <div className="contenedor-login">
                <p>Ingresar</p>
                <div className="formulario-login">
                    <form onSubmit={verifyCount}>
                        <div className="inputs-form-login">
                            <Input_interactive
                                type_input="text"
                                text_input="Correo" 
                                required_input={true}
                                methodChanged={setCorreo} />
                            <Input_interactive
                                type_input="password"
                                text_input="Contraseña" 
                                required_input={true}
                                methodChanged={setClave} />
                        </div>
                        <button>Ir</button>
                    </form>
                </div>
                <a>{textIfNotExistCorreo}</a>
                <div className="options-login">
                    <a href="/Register">Registrar</a>
                    <a href="">He perdido mi contraseña</a>
                </div>
            </div>
        </div>
    )
}

export default Login;