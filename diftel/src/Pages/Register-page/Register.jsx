import { useState, React, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import './src/styles.css'
import Input_interactive from "../../Components-design/Components-unic/Input_interactive";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { async } from "q";

const URL = "http://localhost:8000/";
const URLcontacts = `${URL}contacts/`;
const URLprofesionals = `${URL}profession/`;

// import { Usuario } from './../../FolderClassLibrary/Usuario.js';

function Register() {
    const [contacts, setContact] = useState([]);
    // const [dataContact, setDataContact] = useState(['','','','','','','','','','','']);
    const [textIfExistCorreo, setTextIfExistCorreo] = useState('Correo');

    const [idActual, setidActual] = useState(0);

    const [nombre, setNombre] = useState('');
    const [apellidoPaterno, setApellidoPaterno] = useState('');
    const [apellidoMaterno, setApellidoMaterno] = useState('');
    const [dni, setDni] = useState('');
    const [residenciaActual, setResidenciaActual] = useState('');
    const [telefono, setTelefono] = useState('');
    let [sexo, setSexo] = useState('M');
    let [nacionalidad, setNacionalidad] = useState('Perú');
    let [estadoCivil, setEstadoCivil] = useState('S');
    const [correoPersonal, setCorreoPersonal] = useState('');
    const [fechaNacimiento, setFechaNacimiento] = useState('');
    let [tipoCuenta, setTipoCuenta] = useState('C');
    const [correo, setCorreo] = useState('');
    const [clave, setClave] = useState('');

    const [nombreProfesion, setNombreProfesion] = useState('');
    const [categoriaProfesion, setCategoriaProfesion] = useState('');
    const [aniosExperienciaProfesion, setAniosExperienciaProfesion] = useState(0);
    const [escalaProfesion, setEscalaProfesion] = useState('');

    class ValidationError extends Error{
        constructor(message){
            super(message);
            this.name="ValidationError"

            alert(message);
        }
    }

    const registerContact = async (e) => {
        e.preventDefault();
        try {
            if(aniosExperienciaProfesion < 0){
                throw new ValidationError("Es un número negativo");
            }

            // revisarData();

            await axios.post(URLcontacts, {
                id: 0,
                nombre: nombre,
                apellidoPaterno: apellidoPaterno,
                apellidoMaterno: apellidoMaterno,
                dni: dni,
                residenciaActual: residenciaActual,
                telefono: telefono,
                sexo: (sexo.charAt(0)),
                nacionalidad: nacionalidad,
                estadoCivil: (estadoCivil.charAt(0)),
                correoPersonal: correoPersonal,
                fechaNacimiento: fechaNacimiento,
                tipoCuenta: (tipoCuenta.charAt(0)),
                correo: correo,
                clave: clave
            });

            if (tipoCuenta == 'P') {
                const resc = await axios.get(URLcontacts + `user/${correo}/consult/${clave}`)
                const dataCont = resc.data;
                console.log("Hasta aquí todo correcto");
                dataCont.map((user) => {
                    console.log("user id: " + user.id);
                    setidActual(user.id);
                })
                registerProfesion(e);
            }

            window.location.href = './';

        }catch(error){

        }

    }
    // const revisarData = async () => {
    //     let option_select = null;
    //     if (sexo == '') {
    //         option_select = document.getElementById("getSexo_select_Register");
    //         setSexo(option_select.value);
    //     }
    //     if (nacionalidad == '') {
    //         option_select = document.getElementById("getNacionalidad_select_Register");
    //         setNacionalidad(option_select.value);
    //     }
    //     if (estadoCivil == '') {
    //         option_select = document.getElementById("getEstadoCivil_select_Register");
    //         setEstadoCivil(option_select.value);
    //     }
    //     if (tipoCuenta == '') {
    //         option_select = document.getElementById("getTipoCuenta_select_Register");
    //         setTipoCuenta(option_select.value);
    //     }
    // }

    const verifyCount = async (emailComprob) => {
        const encontrado = await axios.get(URLcontacts + "veriffy/" + emailComprob);
        const dataCont = encontrado.data;
        setCorreo(emailComprob);
        if (Object.keys(dataCont).length != 0) {
            setTextIfExistCorreo('Correo - Precaución, el correo ya existe');
        } else {
            setTextIfExistCorreo('Correo');
        }
        console.log("email: " + emailComprob);
    }

    const registerProfesion = async (e) => {
        e.preventDefault();
        await axios.post(URLprofesionals, {
            // id: 0,
            // nombre: nombreProfesion,
            // categoria: categoriaProfesion,
            // aniosExperiencia: aniosExperienciaProfesion,
            // escala: escalaProfesion,
            // idprofesional: idActual,
            "id": 0,
            "nombre": nombreProfesion,
            "categoria": categoriaProfesion,
            "aniosExperiencia": aniosExperienciaProfesion,
            "escala": escalaProfesion,
            "idprofesional": 52

            // `id`,`nombre`,`categoria`,`aniosExperiencia`
            //,`escala`,`idprofesional`,`createdAt`,`updatedAt`
        });


    }


    return (
        <div className="Register-page-root">
            <form onSubmit={registerContact} >
                <h1>Datos personales</h1>
                <div className="familyName_form_register">
                    <Input_interactive
                        type_input="text"
                        id="fatherName_form_register"
                        text_input="Apellido paterno"
                        required_input={true}
                        methodChanged={setApellidoPaterno} />
                    <Input_interactive
                        type_input="text"
                        id="motherName_password_form_register"
                        text_input="Apellido materno"
                        required_input={true}
                        methodChanged={setApellidoMaterno} />
                    <Input_interactive
                        type_input="text"
                        id="name_form_register"
                        text_input="Nombres"
                        required_input={true}
                        methodChanged={setNombre} />
                </div>
                <div className="familyName_form_register" style={{ marginTop: "30px" }}>
                    <Input_interactive
                        type_input="text"
                        id="dni_form_register"
                        text_input="DNI"
                        required_input={true}
                        methodChanged={setDni} />
                    <Input_interactive
                        type_input="text"
                        id="home_form_register"
                        text_input="Residencia actual"
                        required_input={true}
                        methodChanged={setResidenciaActual} />
                    <Input_interactive
                        type_input="text"
                        id="telephon_form_register"
                        text_input="Teléfono"
                        required_input={true}
                        methodChanged={setTelefono} />
                </div>
                <div className="familyName_form_register" style={{ marginTop: "30px" }}>
                    <div className="Component_selected_interactive_father">
                        <p>Sexo:</p>
                        <select value="M" id="getSexo_select_Register" onChange={(e) => { setSexo(e.target.value) }} required>
                            <option value="M">Masculino</option>
                            <option value="F">Femenino</option>
                            <option value="N">Prefiero no decirlo</option>
                        </select>
                    </div>
                    <div className="Component_selected_interactive_father">
                        <p>Nacionalidad:</p>
                        <select value="Perú" id="getNacionalidad_select_Register" onChange={(e) => { setNacionalidad(e.target.value) }} required>
                            <option value="Perú">Perú</option>
                            <option value="Argentina">Argentina</option>
                            <option value="Bolivia">Bolivia</option>
                            <option value="Brasil">Brasil</option>
                            <option value="Chile">Chile</option>
                            <option value="Colombia">Colombia</option>
                            <option value="Costa">Costa Rica</option>
                            <option value="Cuba">Cuba</option>
                            <option value="Ecuador">Ecuador</option>
                        </select>
                    </div>
                    <div className="Component_selected_interactive_father">
                        <p>Estado civil:</p>
                        <select value="S" id="getEstadoCivil_select_Register" onChange={(e) => { setEstadoCivil(e.target.value) }} required>
                            <option value="S">Soltero(a)</option>
                            <option value="C">Casado(a)</option>
                            <option value="V">Viudo(a)</option>
                            <option value="D">Divorciado(a)</option>
                        </select>
                    </div>
                </div>
                <div className="familyName_form_register" style={{ marginTop: "30px" }}>
                    <Input_interactive
                        type_input="email"
                        id="emailExist_form_register"
                        text_input="Correo personal"
                        required_input={true}
                        methodChanged={setCorreoPersonal} />
                    <div className="date_nacimient_form_register">
                        <p>Fecha de nacimiento:</p>
                        <input type="date" onChange={(e) => { setFechaNacimiento(e.target.value) }} required />
                    </div>
                </div>
                <div className="familyName_form_register" style={{ marginTop: "30px" }}>
                    <div className="Component_selected_interactive_father">
                        <p>Tipo de cuenta:</p>
                        <select defaultValue="E" id="getTipoCuenta_select_Register" onChange={(e) => { console.log(e.target.value); setTipoCuenta(e.target.value) }} required>
                            <option value="C">Cliente</option>
                            <option value="P">Profesional</option>
                        </select>
                    </div>
                </div>
                <div className="familyName_form_register profesionalSpecc_form_register" style={{ marginTop: "30px", display: ` ${(tipoCuenta == 'P') ? "flex" : "none"}` }}>
                    <Input_interactive
                        type_input="text"
                        id="profesion_form_register"
                        text_input="Profesión"
                        required_input={false}
                        methodChanged={setNombreProfesion} />
                    <Input_interactive
                        type_input="text"
                        id="categoria_password_form_register"
                        text_input="Categoría"
                        required_input={false}
                        methodChanged={setCategoriaProfesion} />
                    <Input_interactive
                        type_input="number"
                        id="experiencia_form_register"
                        text_input="Experiencia"
                        required_input={false}
                        methodChanged={setAniosExperienciaProfesion} />
                    <Input_interactive
                        type_input="text"
                        id="escala_form_register"
                        text_input="Escala"
                        required_input={false}
                        methodChanged={setEscalaProfesion} />
                </div>
                <h1>Crear cuenta</h1>
                <div className="createCount-form-Register">
                    <Input_interactive
                        type_input="email"
                        id="id_count_form_register"
                        text_input={textIfExistCorreo}
                        required_input={true}
                        methodChanged={verifyCount} />
                    <Input_interactive
                        type_input="password"
                        id="id_password_form_register"
                        text_input="Contraseña"
                        required_input={true}
                        methodChanged={setClave} />
                </div>
                <button>Enviar</button>
            </form>
        </div>
    )
}
export default Register;
