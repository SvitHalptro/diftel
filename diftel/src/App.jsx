import React from "react";
import { BrowserRouter , Routes, Route} from "react-router-dom";
import Login from "./Pages/Login-page/Login";
import Register from "./Pages/Register-page/Register";
import Home from "./Pages/Home-page/Home";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login/>} />
        <Route path="/Register" element={<Register/>} />
        <Route path="/Home" element={<Home/>} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
