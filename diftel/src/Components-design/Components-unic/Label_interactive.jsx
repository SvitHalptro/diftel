import React from "react";
import './Label_interactive_style.css'

function Label_interactive({ text_label, valueText}) {
    return (
        <div className="Component_label_interactive_father">
            <label className="Component_label_interactive_email-login" />{valueText}<label/>
            <span className="Component_label_interactive_span-label-login">{text_label}</span>
        </div>
    );
}
export default Label_interactive;