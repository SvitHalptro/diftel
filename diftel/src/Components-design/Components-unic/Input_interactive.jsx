import React from "react";
import './Input_interactive_style.css'

function Input_interactive({type_input, id_input, text_input, required_input, methodChanged}) {
    return (
        <div className="Component_input_interactive_father">
            {required_input ? <input type={`${type_input}`} onChange={ (e)=> methodChanged(e.target.value) }  className="Component_input_interactive_email-login" required/> : <input type={`${type_input}`} onChange={ (e)=> methodChanged(e.target.value) } className="Component_input_interactive_email-login"/>}
            <span className="Component_input_interactive_span-label-login">{text_input}</span>
        </div>
    );
}
export default Input_interactive;
// id={`${id_input}`}