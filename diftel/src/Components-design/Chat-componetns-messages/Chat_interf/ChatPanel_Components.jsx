import './ChatPanel_Components_style.css';
import imgContact from './../../../src/img/iconLog.png'
import { Message_Components } from '../Message_interf/MessagePanel_Components';
import { useEffect, useState } from 'react';
import Cookies from 'universal-cookie';
import axios from 'axios';
import { ChatMessages } from './ChatPanel2/ChatMessages';
import { MeetingType } from './DateConference/MeetingType';

const URL = "http://localhost:8000/";
const URLcontacts = `${URL}contacts/`;
const URLprofesionals = `${URL}profession/`;
const URLmessages = `${URL}getMessages/`;


export function ChatPanel_Components({ Nombre, apellidos, id, methodActualizarMensajes, totalMessages, meetings_total}) {
    const cookies = new Cookies();
    
    useEffect(() => {
        methodActualizarMensajes();
    }, []);

    const envMessage = async () => {
        const inputText = document.getElementById('inputMessage_env');
        if ((inputText.value).length != 0) {
            await axios.post(URLmessages, {
                "id": 0,
                "mensaje": inputText.value,
                "fecha": Date.now(),
                "idEmisor": cookies.get('idUser'),
                "idReceptor": id
            })
        }
        methodActualizarMensajes();
    }
    

    return (
        <div className="Chat_panel_ChatPanel_Components">
            <div className="partSup_ChatPanel_Components">
                <img src={imgContact} alt="No image" />
                <p>{Nombre + ' ' + apellidos}</p>
            </div>

            {/* {(meetings_total.length > 0) ? */}
                < ChatMessages
                    Nombre={Nombre}
                    apellidos={apellidos}
                    id={id}
                    methodActualizarMensajes={methodActualizarMensajes}
                    totalMessages={totalMessages} />
                {/* : <MeetingType></MeetingType>
            } */}

            <div className="partEnvMessage_ChatPanel_Components">
                <input type="text" id='inputMessage_env' />
                <button onClick={envMessage}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-send" viewBox="0 0 16 16">
                        <path d="M15.854.146a.5.5 0 0 1 .11.54l-5.819 14.547a.75.75 0 0 1-1.329.124l-3.178-4.995L.643 7.184a.75.75 0 0 1 .124-1.33L15.314.037a.5.5 0 0 1 .54.11ZM6.636 10.07l2.761 4.338L14.13 2.576 6.636 10.07Zm6.787-8.201L1.591 6.602l4.339 2.76 7.494-7.493Z" />
                    </svg>
                </button>
            </div>
        </div>
    )
} 