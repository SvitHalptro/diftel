import Cookies from "universal-cookie"
import { Message_Components } from "../../Message_interf/MessagePanel_Components";

export const ChatMessages = ({ Nombre, apellidos, id , methodActualizarMensajes, totalMessages})=> {
    const cookies = new Cookies(); 
    
    return(
        <div className="partMessages_ChatPanel_Components" id='partMessages_ChatPanel_Components'>
                {
                    (totalMessages.map((message_u) => (
                        <Message_Components
                            message={message_u.mensaje}
                            dateEnvRec={message_u.fecha}
                            isEnv={(message_u.idEmisor == cookies.get('idUser')) ? true : false} />
                    )))
                }
        </div>
    )
}