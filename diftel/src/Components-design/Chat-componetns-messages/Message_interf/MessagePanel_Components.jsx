import './MessagePanel_Components_style.css';

export function Message_Components({message, dateEnvRec, isEnv}) {
    
    return (
        <div className="message_panel_Message_Components">
            
            <div style={{backgroundColor: `${isEnv ? '#03a582' : '#00aadd'}`}}>
                <label>{message}</label>
            </div>
            <span>{dateEnvRec}</span>
        </div>
    )
} 