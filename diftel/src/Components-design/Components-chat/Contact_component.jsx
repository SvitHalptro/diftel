import React from "react";
import './Contact_component_style.css'
import imgSilueta from './../../src/img/iconLog.png'

function Contact_component({nameContact, idContact, methodContactClick}){
    return(
        <div className="root_contact_component" id={idContact} >
            <img src={imgSilueta} alt={idContact} onClick={(e)=> methodContactClick(e.target.alt)}/>
            <p className={idContact} onClick={(e)=> methodContactClick(e.target.className)}>{nameContact}</p>
        </div>
    );
}
export default Contact_component;