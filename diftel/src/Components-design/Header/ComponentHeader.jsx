import React from "react";
import './ComponentHeaderStyle.css'
import Cookies from 'universal-cookie';
import imgIcon from './../../src/img/DIFTEL.png';

function ComponentHeader({ numPress, functionCambPanel }) {
    const cookies = new Cookies();
    const destroyCount = ()=>{
        cookies.remove("idUser");
        window.location.href = './';
    }

    return (
        <header className="header_component_header">
            <nav className="options_component_header">
                <img src={imgIcon} alt="No image" />
                <ul>
                    <li onClick={()=>functionCambPanel(1)} style={{ borderBottom: `${(numPress==1) ? "2px solid #00dd00" : ""}` }}>Explorar</li>
                    <li onClick={()=>functionCambPanel(2)} style={{ borderBottom: `${(numPress==2) ? "2px solid #00dd00" : ""}` }}>Mi cuenta</li>
                    <li onClick={()=>destroyCount()}>Salir</li>
                </ul>
            </nav>
        </header>
    );
}

export default ComponentHeader;